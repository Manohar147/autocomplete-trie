package autocomplete;

import java.util.Collection;
import java.util.List;

/**
 * @param <T> all sub classes of {@link String}
 * @author muraliveeravalli
 * @apiNote {@link AutoComplete} makes use of {@link Dictionary} Data Structure to perform efficient auto complete
 * Functionality.
 * The Time Complexity to perform an auto complete operation with the help of {@link Dictionary} DS is O(Length OF words) + O(No. Of words starting with the prefix)
 * Thus helps in following
 * 1) Reduce Load on DB for every request.
 * 2) Efficient retrieval of words
 * @see Trie
 * @see Dictionary
 * @see Collection
 * @see Iterable
 */
public interface AutoComplete<T extends String> extends Collection<T> {

    /**
     * The No Of Characters In a Given {@link Trie} Node. Each number represents a unique {@code Node}.
     */
    int SIZE = 96;
    /**
     * The WHITE SPACE Character is used a reference for insertion and retrieval of {@code Node}.
     */
    char DELIMITER = ' ';
    /**
     * Represents EMPTY Character.
     */
    String EMPTY = "";
    /**
     * Default Length Used while retrieval of words.
     */
    int DEFAULT_LENGTH = 50;
    /**
     * The Initial height of {@link Trie}
     */
    int LEVEL_ZERO = 0;

    /**
     * Gives the list of words in {@link Dictionary} that start with a given prefix.
     *
     * @param prefix {@link String} the prefix which is to be queried.
     * @return {@link List<String>} the words starting with the prefix.
     */
    List<T> getAutoSuggestions(T prefix);

    /**
     * Gives all the words in the {@link Dictionary}.
     *
     * @return {@link List<String>} all the words in the {@link Dictionary}.
     */
    List<T> getAutoSuggestions();

    /**
     * @return the length of the longest word in {@link Dictionary}
     */
    int getMaxWordLength();

    /**
     * @return the longest prefix present in the {@link Dictionary}
     */
    String longestPrefix();
}
