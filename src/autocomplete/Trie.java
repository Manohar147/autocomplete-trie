package autocomplete;

/**
 * @author muraliveeravalli
 * @apiNote {@link Trie} is an N Ary Tree. where each node represents a {@code Character}.
 * @see {@link Dictionary}
 * @see {@link AutoComplete}
 */
public class Trie {

    public boolean eof;
    public Trie[] next;

    public Trie(int size) {
        next = new Trie[size];
    }
}
