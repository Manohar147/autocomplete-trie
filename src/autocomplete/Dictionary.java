package autocomplete;

import java.util.*;

/**
 * @author muraliveeravalli
 * @apiNote The {@code Dictionary} Collection helps to implement a {@link Trie} based Data Structure to provide quick and efficient autocomplete
 * functionality.
 * The Time complexity of inserting a word in {@code Dictionary} is O(Length of the word).
 * The Time complexity of deleting a word in {@code Dictionary} is O(Length of the word).
 * The Time complexity of searching a word in {@code Dictionary} is O(Length of the word).
 * The Time complexity of retrieving a words in {@code Dictionary} is O(N).
 * The Time complexity of retrieving a words with a prefix in {@code Dictionary} is O(N).
 * Thus the {@code Dictionary} DS helps in efficient searching and retrieval of data.
 * @see Trie
 * @see AutoComplete
 * @see Collection
 * @see Iterable
 */
public class Dictionary<T extends String> implements AutoComplete<T> {

    /**
     * {@link Integer}The SIZE of the {@link Dictionary}
     */
    private int size;
    /**
     * {@link Trie} the ROOT of the {@link Dictionary}
     */
    private Trie root;
    /**
     * {@link Trie} The Length of the longest word in {@link Dictionary}
     */
    private int max_length;

    /**
     * @param words {@link Collection<T> The data which needs to be inserted in Dictionary}
     */
    public Dictionary(Collection<T> words) {
        root = new Trie(SIZE);
        addAll(words);
    }

    public Dictionary() {
        this(null);
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return isEmpty(root);
    }

    @Override
    public Iterator<T> iterator() {
        return getAutoSuggestions().iterator();
    }

    @Override
    public Object[] toArray() {
        return getAutoSuggestions().toArray();
    }


    @Override
    public boolean removeAll(Collection<?> c) {
        if (null == c) {
            return false;
        }
        for (Object s : c) {
            if (!remove(s)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        if (null == c) {
            return false;
        }
        return getAutoSuggestions().retainAll(c);
    }

    @Override
    public void clear() {
        size = 0;
        root = null;
        root = new Trie(SIZE);
    }


    @Override
    public boolean contains(Object o) {
        String key = (String) o;
        int index;
        Trie t = root;
        for (int level = 0; level < key.length(); level++) {
            index = key.charAt(level) - DELIMITER;
            if (t.next[index] == null) {
                return false;
            }
            t = t.next[index];
        }
        return (t != null && t.eof);
    }


    @Override
    public boolean addAll(Collection<? extends T> c) {
        if (null == c) {
            return false;
        }
        for (String s : c) {
            if (!add(s)) {
                return false;
            }
        }
        return true;
    }


    @Override
    public <T> T[] toArray(T[] a) {
        return (T[]) getAutoSuggestions().toArray();
    }

    @Override
    public boolean add(String s) {
        if (contains(s)) {
            return false;
        }
        int index;
        Trie t = root;
        for (int level = 0; level < s.length(); level++) {
            index = s.charAt(level) - DELIMITER;
            if (t.next[index] == null)
                t.next[index] = new Trie(SIZE);

            t = t.next[index];
        }
        t.eof = true;
        if (s.length() > max_length) {
            max_length = s.length();
        }
        size++;
        return true;
    }


    @Override
    public boolean remove(Object o) {
        if (!contains(o)) {
            return false;
        }
        remove(root, (String) o, 0);
        size--;
        max_length = calculateMaxWordLength();
        return true;
    }

    /**
     * {@code remove} method is used to remove a word for {@code Dictionary}
     *
     * @param root  {@link Trie} the root of {@code Dictionary}
     * @param key   {@link String} the word to be removed.
     * @param depth {@link int} height of tree
     * @return {@link Trie}
     */
    private Trie remove(Trie root, String key, int depth) {
        if (root == null)
            return null;

        if (depth == key.length()) {
            if (root.eof)
                root.eof = false;
            if (isEmpty(root)) {
                root = null;
            }
            return root;
        }

        int index = key.charAt(depth) - DELIMITER;
        root.next[index] = remove(root.next[index], key, depth + 1);
        if (isEmpty(root) && !root.eof) {
            root = null;
        }
        return root;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object s : c) {
            if (!contains(s)) {
                return false;
            }
        }
        return true;
    }

    /**
     * {@code autoSuggestions} method is a helper method which is used to retrieve all words in the {@code Dictionary}.
     *
     * @param u      {@link Class} to indicate what kind of data is to be returned.
     *               Data: {@link Integer} -> where the data is the length of each word inserted in {@code Dictionary}.
     *               Data: {@link String} -> where the data is word itself.
     * @param root   {@link Trie} the root of {@code Dictionary}
     * @param str    {@link char[]}  the array used to recursively retrieve the words from the {@code Dictionary}.
     * @param level  {@link int} indicated the height of the {@link Trie}
     * @param o      {@link Collection<U>} used for data retrieval
     * @param prefix {@link String} used prefix of word.
     * @param <U>    used to indicate the type of data.
     */
    private <U> void autoSuggestions(Class<U> u, Trie root, char[] str, int level, Collection<U> o, String prefix) {
        if (root.eof) {
            str[level] = '\0';
            String word = prefix;
            for (char c : str) {
                if (c != '\0') {
                    word += c;
                } else {
                    break;
                }
            }
            if (Integer.class.equals(u)) {
                o.add((U) (Integer) (word.length()));
            } else {
                o.add((U) word);
            }
        }
        for (int i = 0; i < root.next.length; i++) {
            if (null != root.next[i]) {
                str[level] = (char) (i + DELIMITER);
                autoSuggestions(u, root.next[i], str, level + 1, o, prefix);
            }
        }
    }

    /**
     * {@code isEmpty} used to check if the {@code Dictionar} is empty.
     *
     * @param root {@link Trie} root of thr {@code Dictionary}.
     * @return {@link Boolean} {@code true} if the {@code Dictionary} is empty.
     */
    private boolean isEmpty(Trie root) {
        if (null == root) {
            return false;
        }
        for (int i = 0; i < root.next.length; i++) {
            if (root.next[i] != null)
                return false;
        }
        return true;
    }

    @Override
    public List<T> getAutoSuggestions(String query) {
        int i = 0;
        Trie t = root;
        List<String> o = new LinkedList<>();
        if (null != query) {
            while (i < query.length()) {
                if (t.next[query.charAt(i) - DELIMITER] == null) {
                    return (List<T>) o;
                }
                t = t.next[query.charAt(i) - DELIMITER];
                i++;
            }
        }
        autoSuggestions(String.class, t, new char[max_length + DEFAULT_LENGTH], LEVEL_ZERO, o, query);
        return (List<T>) o;
    }

    @Override
    public List<T> getAutoSuggestions() {
        return getAutoSuggestions(EMPTY);
    }

    @Override
    public int getMaxWordLength() {
        return max_length;
    }

    /**
     * @return {@link Integer} length of maximum word in {@code Dictionary}
     */
    private int calculateMaxWordLength() {
        TreeSet<Integer> counts = new TreeSet<>();
        autoSuggestions(Integer.class, root, new char[max_length + DEFAULT_LENGTH], LEVEL_ZERO, counts, EMPTY);
        return counts.last();
    }

    @Override
    public String longestPrefix() {
        return longestPrefixHelper(root, EMPTY);
    }

    /**
     * {@code longestPrefixHelper} is a helper method used to find the longest prefix in {@link Dictionary}
     *
     * @param root   {@link Trie} the root of the {@link Dictionary}
     * @param prefix {@link String} the longest prefix in {@link Dictionary}
     * @return {@link String} the longest prefix in {@link Dictionary}
     */
    private String longestPrefixHelper(Trie root, String prefix) {
        if (root == null) {
            return prefix;
        }
        boolean valid = true;
        int index = 0;
        StringBuilder prefixBuilder = new StringBuilder(prefix);
        for (int i = 0; i < root.next.length; i++) {
            if (!valid && root.next[i] != null) {
                return EMPTY;
            } else if (root.next[i] != null) {
                valid = false;
                index = i;
                prefixBuilder.append((char) (index + DELIMITER));
            }
        }
        prefix = prefixBuilder.toString();
        return prefix + longestPrefixHelper(root.next[index], EMPTY);
    }
}
