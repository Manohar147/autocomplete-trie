import autocomplete.Dictionary;
import autocomplete.AutoComplete;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        AutoComplete<String> c = new Dictionary<>();
        List<String> l = new ArrayList<>();
        c.addAll(l);
        System.out.println(c.longestPrefix());
        System.out.println(c.getAutoSuggestions(""));
        System.out.println(c.size() + ", " + l.size());
        l.remove("hell");
        c.remove("hell");
        System.out.println(c.getAutoSuggestions("hel"));
        System.out.println(c.size() + ", " + l.size());

    }
}
